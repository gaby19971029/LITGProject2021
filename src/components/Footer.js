import React from 'react';

const Footer = () => {
    return (
        <div>
            <footer class="footer mt-auto py-3" id="bg-light">
                <div class="container" >
                    <span class="text-muted">Gabriel Clemente Copyright 2021</span>
                </div>
            </footer>
        </div>
    )
}

export default Footer;
